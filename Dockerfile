FROM golang:alpine as builder
ENV GO111MODULE=on

WORKDIR /app

COPY . .

RUN go mod tidy

RUN go build -o ./run ./cmd/service
    
FROM alpine:latest

WORKDIR /app

COPY --from=builder /app/run .
COPY --from=builder /app/api/swagger.yml ./api/

ENTRYPOINT ["./run", "-config_path",  "/app/configs/config.yml"]