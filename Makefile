GEN_DIR=internal/gen
SERVICE_NAME = schema-calc-service
SERVICE_PORT = 8001

swagger-gen:
	if ! [ -d $(GEN_DIR) ]; then \
	    mkdir $(GEN_DIR); \
	elif [ -d $(GEN_DIR) ]; then \
		rm -rf $(GEN_DIR); \
		mkdir $(GEN_DIR); \
	fi && \
	swagger generate server -t internal/gen -f ./api/swagger.yml --exclude-main -A $(SERVICE_NAME) && \
	go mod tidy && \
	git add $(GEN_DIR)

go-run+build-dev:
	go build ./cmd/service/ &&\
	sudo ./service -config_path="$(PWD)/configs/dev.yml"

docker-build:
	docker build -t $(SERVICE_NAME) .

docker-run-dev:
	docker run -v $(PWD)/configs/dev.yml:/app/configs/config.yml -v $(PWD)/logs:/app/logs --network host -d --restart=always --name $(SERVICE_NAME) $(SERVICE_NAME)
