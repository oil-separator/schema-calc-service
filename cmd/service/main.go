package main

import (
	"fmt"
	"os"
	"os/signal"
	"service/internal/config"
	"service/internal/cors"
	"service/internal/features/logging"
	"service/internal/http_server"
	"service/internal/schema"
	"syscall"
	"time"

	"github.com/pkg/errors"
)

func mainError(err error, log *logging.Logger) {
	err = errors.Wrap(err, "[main()]")
	if log != nil {
		(*log).Error(err.Error())
	} else {
		fmt.Println("ERROR:", err.Error())
	}

	panic(err)
}

func main() {
	// Заводим канал для ожидания сигнала со стороны os
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

	// Подготавливаем конфиг
	conf, err := config.New()
	if err != nil {
		err = errors.Wrap(err, "unable to create config: [config.New]")
		panic(err)
	}

	// Подготавливаем логгирование
	logConfig := logging.NewConfig()
	logConfig.SetInFile(conf.Logger.LogInFile)
	logConfig.SetOutputDir(conf.Logger.OutputDir)
	logConfig.SetLevel(logging.InfoLevel)
	log, err := logging.New(logConfig)
	if err != nil {
		err = errors.Wrap(err, "unable to create logging: [logging.New()]")
		panic(err)
	}

	s := schema.NewSchema()

	out := s.NewOutput(0)

	v1 := s.NewValve(0, 0)
	v1.Block(1)

	schema.Connect(0, 0, out, v1)

	v2 := s.NewValve(1, 1)
	v2.Block(1)

	schema.Connect(1, 0, v1, v2)

	sep := s.NewSeparator(0, 20000)
	// ---
	sepOilLevel := s.NewInfo(0)
	sepOilLevel.SetFunc(func() error {
		sepOilLevel.SetValue(sep.OilLevel())
		return nil
	})
	sep.AddInfo(sepOilLevel)

	sepMezhFaz := s.NewInfo(1)
	sepMezhFaz.SetFunc(func() error {
		sepMezhFaz.SetValue(sep.MezhFaz())
		return nil
	})
	sep.AddInfo(sepMezhFaz)

	sepDavlenie1 := s.NewInfo(2)
	sepDavlenie1.SetFunc(func() error {
		sepDavlenie1.SetValue(sep.Davlenie1())
		return nil
	})
	sep.AddInfo(sepDavlenie1)

	sepDavlenie2 := s.NewInfo(3)
	sepDavlenie2.SetFunc(func() error {
		sepDavlenie2.SetValue(sep.Davlenie2())
		return nil
	})
	sep.AddInfo(sepDavlenie2)

	sepTemp := s.NewInfo(4)
	sepTemp.SetFunc(func() error {
		sepTemp.SetValue(sep.Temp())
		return nil
	})
	sep.AddInfo(sepTemp)

	sepPerepadDavl := s.NewInfo(5)
	sepPerepadDavl.SetFunc(func() error {
		sepPerepadDavl.SetValue(sep.PerepadDavl())
		return nil
	})
	sep.AddInfo(sepPerepadDavl)

	sepUrNeftGaz1 := s.NewInfo(6)
	sepUrNeftGaz1.SetFunc(func() error {
		sepUrNeftGaz1.SetValue(sep.UrNeftGaz1())
		return nil
	})
	sep.AddInfo(sepUrNeftGaz1)

	sepUrNeftGaz2 := s.NewInfo(7)
	sepUrNeftGaz2.SetFunc(func() error {
		sepUrNeftGaz2.SetValue(sep.UrNeftGaz2())
		return nil
	})
	sep.AddInfo(sepUrNeftGaz2)
	// ---

	schema.Connect(1, 0, v1, sep)

	gasv1 := s.NewValve(2, 1)
	gasv1.Block(1)

	schema.Connect(0, 0, sep, gasv1)

	gasv2 := s.NewValve(3, 0)
	gasv2.Block(1)

	schema.Connect(0, 0, gasv1, gasv2)

	inpGas := s.NewInput(0)

	schema.Connect(0, 0, gasv2, inpGas)

	dv1_1 := s.NewValve(4, 0)
	dv1_1.Block(1)

	schema.Connect(1, 0, sep, dv1_1)

	dv2_4 := s.NewValve(5, 0)
	dv2_4.Block(1)

	schema.Connect(4, 0, sep, dv2_4)

	drenageInp := s.NewInput(1)

	schema.Connect(0, 0, dv1_1, drenageInp)
	schema.Connect(0, 0, dv2_4, drenageInp)

	vWater2 := s.NewValve(6, 0)
	vWater2.Block(1)
	infVWater2 := s.NewInfo(9)
	infVWater2.SetFunc(func() error {
		infVWater2.SetValue(vWater2.GetBlockLvl())
		return nil
	})
	vWater2.SetInfo(infVWater2)

	schema.Connect(2, 0, sep, vWater2)

	waterInp := s.NewInput(2)

	schema.Connect(0, 0, vWater2, waterInp)

	v1ClearOil3 := s.NewValve(7, 0)
	v1ClearOil3.Block(1)
	infV1ClearOil3 := s.NewInfo(8)
	infV1ClearOil3.SetFunc(func() error {
		infV1ClearOil3.SetValue(v1ClearOil3.GetBlockLvl())
		return nil
	})
	v1ClearOil3.SetInfo(infV1ClearOil3)

	schema.Connect(3, 0, sep, v1ClearOil3)

	v2ClearOil3 := s.NewValve(8, 0)
	v2ClearOil3.Block(1)

	schema.Connect(3, 0, v1ClearOil3, v2ClearOil3)

	clearOilInp := s.NewInput(2)

	schema.Connect(0, 0, v2ClearOil3, clearOilInp)

	go func() {
		t := time.NewTicker(time.Second)
		i := 0
		for t := range t.C {
			t = t

			let := schema.NewLetter(0)
			let.AddContent(schema.NewCrudeOil(), 200)
			out.Write(*let)
			i++
		}
	}()

	serv := http_server.NewHttpServer(
		conf.Service.Host,
		conf.Service.Port,
		s,
		log,
	)

	// Запускаем прокси с использованием cors
	log.Info("start cors")
	cors := cors.NewCors(conf.Cors.Target, conf.Cors.Path)
	cors.Run()

	serv.Start()

	<-quit

	serv.Stop()
}
