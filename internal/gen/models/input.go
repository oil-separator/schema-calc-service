// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"strconv"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// Input input
//
// swagger:model input
type Input struct {

	// content
	Content []*InputContentItems0 `json:"content"`

	// id
	ID uint64 `json:"id,omitempty"`

	// x cord
	XCord float64 `json:"x_cord,omitempty"`

	// y cord
	YCord float64 `json:"y_cord,omitempty"`

	// z index
	ZIndex uint64 `json:"z_index,omitempty"`
}

// Validate validates this input
func (m *Input) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateContent(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *Input) validateContent(formats strfmt.Registry) error {

	if swag.IsZero(m.Content) { // not required
		return nil
	}

	for i := 0; i < len(m.Content); i++ {
		if swag.IsZero(m.Content[i]) { // not required
			continue
		}

		if m.Content[i] != nil {
			if err := m.Content[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("content" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

// MarshalBinary interface implementation
func (m *Input) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Input) UnmarshalBinary(b []byte) error {
	var res Input
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// InputContentItems0 input content items0
//
// swagger:model InputContentItems0
type InputContentItems0 struct {

	// amount
	Amount float64 `json:"amount,omitempty"`

	// value
	Value string `json:"value,omitempty"`
}

// Validate validates this input content items0
func (m *InputContentItems0) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *InputContentItems0) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *InputContentItems0) UnmarshalBinary(b []byte) error {
	var res InputContentItems0
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
