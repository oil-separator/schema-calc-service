// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"strconv"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// Output output
//
// swagger:model output
type Output struct {

	// id
	ID uint64 `json:"id,omitempty"`

	// output values
	OutputValues []*OutputOutputValuesItems0 `json:"output_values"`

	// x cord
	XCord float64 `json:"x_cord,omitempty"`

	// y cord
	YCord float64 `json:"y_cord,omitempty"`

	// z index
	ZIndex uint64 `json:"z_index,omitempty"`
}

// Validate validates this output
func (m *Output) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateOutputValues(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *Output) validateOutputValues(formats strfmt.Registry) error {

	if swag.IsZero(m.OutputValues) { // not required
		return nil
	}

	for i := 0; i < len(m.OutputValues); i++ {
		if swag.IsZero(m.OutputValues[i]) { // not required
			continue
		}

		if m.OutputValues[i] != nil {
			if err := m.OutputValues[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("output_values" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

// MarshalBinary interface implementation
func (m *Output) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Output) UnmarshalBinary(b []byte) error {
	var res Output
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// OutputOutputValuesItems0 output output values items0
//
// swagger:model OutputOutputValuesItems0
type OutputOutputValuesItems0 struct {

	// flow
	Flow float64 `json:"flow,omitempty"`

	// value
	Value string `json:"value,omitempty"`
}

// Validate validates this output output values items0
func (m *OutputOutputValuesItems0) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *OutputOutputValuesItems0) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *OutputOutputValuesItems0) UnmarshalBinary(b []byte) error {
	var res OutputOutputValuesItems0
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
