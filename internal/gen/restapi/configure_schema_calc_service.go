// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"service/internal/gen/restapi/operations"
	"service/internal/gen/restapi/operations/input"
	"service/internal/gen/restapi/operations/output"
	"service/internal/gen/restapi/operations/pipe_connector"
	"service/internal/gen/restapi/operations/separator"
	"service/internal/gen/restapi/operations/shema"
	"service/internal/gen/restapi/operations/valve"
)

//go:generate swagger generate server --target ../../gen --name SchemaCalcService --spec ../../../api/swagger.yml --principal interface{} --exclude-main

func configureFlags(api *operations.SchemaCalcServiceAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.SchemaCalcServiceAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	if api.InputObjectInputGetHandler == nil {
		api.InputObjectInputGetHandler = input.ObjectInputGetHandlerFunc(func(params input.ObjectInputGetParams) middleware.Responder {
			return middleware.NotImplemented("operation input.ObjectInputGet has not yet been implemented")
		})
	}
	if api.OutputObjectOutputGetHandler == nil {
		api.OutputObjectOutputGetHandler = output.ObjectOutputGetHandlerFunc(func(params output.ObjectOutputGetParams) middleware.Responder {
			return middleware.NotImplemented("operation output.ObjectOutputGet has not yet been implemented")
		})
	}
	if api.PipeConnectorObjectPipeConnectorGetHandler == nil {
		api.PipeConnectorObjectPipeConnectorGetHandler = pipe_connector.ObjectPipeConnectorGetHandlerFunc(func(params pipe_connector.ObjectPipeConnectorGetParams) middleware.Responder {
			return middleware.NotImplemented("operation pipe_connector.ObjectPipeConnectorGet has not yet been implemented")
		})
	}
	if api.SeparatorObjectSeparatorGetHandler == nil {
		api.SeparatorObjectSeparatorGetHandler = separator.ObjectSeparatorGetHandlerFunc(func(params separator.ObjectSeparatorGetParams) middleware.Responder {
			return middleware.NotImplemented("operation separator.ObjectSeparatorGet has not yet been implemented")
		})
	}
	if api.ValveObjectValveLvlPostHandler == nil {
		api.ValveObjectValveLvlPostHandler = valve.ObjectValveLvlPostHandlerFunc(func(params valve.ObjectValveLvlPostParams) middleware.Responder {
			return middleware.NotImplemented("operation valve.ObjectValveLvlPost has not yet been implemented")
		})
	}
	if api.ValveObjectValveGetHandler == nil {
		api.ValveObjectValveGetHandler = valve.ObjectValveGetHandlerFunc(func(params valve.ObjectValveGetParams) middleware.Responder {
			return middleware.NotImplemented("operation valve.ObjectValveGet has not yet been implemented")
		})
	}
	if api.ShemaShemaMetricsGetHandler == nil {
		api.ShemaShemaMetricsGetHandler = shema.ShemaMetricsGetHandlerFunc(func(params shema.ShemaMetricsGetParams) middleware.Responder {
			return middleware.NotImplemented("operation shema.ShemaMetricsGet has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
