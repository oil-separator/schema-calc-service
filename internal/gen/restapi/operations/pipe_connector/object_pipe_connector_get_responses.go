// Code generated by go-swagger; DO NOT EDIT.

package pipe_connector

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"service/internal/gen/models"
)

// ObjectPipeConnectorGetOKCode is the HTTP code returned for type ObjectPipeConnectorGetOK
const ObjectPipeConnectorGetOKCode int = 200

/*
ObjectPipeConnectorGetOK Возвращаемые ответы

swagger:response objectPipeConnectorGetOK
*/
type ObjectPipeConnectorGetOK struct {

	/*
	  In: Body
	*/
	Payload []*models.PipeConnector `json:"body,omitempty"`
}

// NewObjectPipeConnectorGetOK creates ObjectPipeConnectorGetOK with default headers values
func NewObjectPipeConnectorGetOK() *ObjectPipeConnectorGetOK {

	return &ObjectPipeConnectorGetOK{}
}

// WithPayload adds the payload to the object pipe connector get o k response
func (o *ObjectPipeConnectorGetOK) WithPayload(payload []*models.PipeConnector) *ObjectPipeConnectorGetOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the object pipe connector get o k response
func (o *ObjectPipeConnectorGetOK) SetPayload(payload []*models.PipeConnector) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *ObjectPipeConnectorGetOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	payload := o.Payload
	if payload == nil {
		// return empty array
		payload = make([]*models.PipeConnector, 0, 50)
	}

	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}

// ObjectPipeConnectorGetInternalServerErrorCode is the HTTP code returned for type ObjectPipeConnectorGetInternalServerError
const ObjectPipeConnectorGetInternalServerErrorCode int = 500

/*
ObjectPipeConnectorGetInternalServerError Ошибка сервера

swagger:response objectPipeConnectorGetInternalServerError
*/
type ObjectPipeConnectorGetInternalServerError struct {

	/*
	  In: Body
	*/
	Payload *models.Error500 `json:"body,omitempty"`
}

// NewObjectPipeConnectorGetInternalServerError creates ObjectPipeConnectorGetInternalServerError with default headers values
func NewObjectPipeConnectorGetInternalServerError() *ObjectPipeConnectorGetInternalServerError {

	return &ObjectPipeConnectorGetInternalServerError{}
}

// WithPayload adds the payload to the object pipe connector get internal server error response
func (o *ObjectPipeConnectorGetInternalServerError) WithPayload(payload *models.Error500) *ObjectPipeConnectorGetInternalServerError {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the object pipe connector get internal server error response
func (o *ObjectPipeConnectorGetInternalServerError) SetPayload(payload *models.Error500) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *ObjectPipeConnectorGetInternalServerError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(500)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
