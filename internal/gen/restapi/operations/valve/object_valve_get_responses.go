// Code generated by go-swagger; DO NOT EDIT.

package valve

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"service/internal/gen/models"
)

// ObjectValveGetOKCode is the HTTP code returned for type ObjectValveGetOK
const ObjectValveGetOKCode int = 200

/*
ObjectValveGetOK Возвращаемые ответы

swagger:response objectValveGetOK
*/
type ObjectValveGetOK struct {

	/*
	  In: Body
	*/
	Payload []*models.Valve `json:"body,omitempty"`
}

// NewObjectValveGetOK creates ObjectValveGetOK with default headers values
func NewObjectValveGetOK() *ObjectValveGetOK {

	return &ObjectValveGetOK{}
}

// WithPayload adds the payload to the object valve get o k response
func (o *ObjectValveGetOK) WithPayload(payload []*models.Valve) *ObjectValveGetOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the object valve get o k response
func (o *ObjectValveGetOK) SetPayload(payload []*models.Valve) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *ObjectValveGetOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	payload := o.Payload
	if payload == nil {
		// return empty array
		payload = make([]*models.Valve, 0, 50)
	}

	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}

// ObjectValveGetInternalServerErrorCode is the HTTP code returned for type ObjectValveGetInternalServerError
const ObjectValveGetInternalServerErrorCode int = 500

/*
ObjectValveGetInternalServerError Ошибка сервера

swagger:response objectValveGetInternalServerError
*/
type ObjectValveGetInternalServerError struct {

	/*
	  In: Body
	*/
	Payload *models.Error500 `json:"body,omitempty"`
}

// NewObjectValveGetInternalServerError creates ObjectValveGetInternalServerError with default headers values
func NewObjectValveGetInternalServerError() *ObjectValveGetInternalServerError {

	return &ObjectValveGetInternalServerError{}
}

// WithPayload adds the payload to the object valve get internal server error response
func (o *ObjectValveGetInternalServerError) WithPayload(payload *models.Error500) *ObjectValveGetInternalServerError {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the object valve get internal server error response
func (o *ObjectValveGetInternalServerError) SetPayload(payload *models.Error500) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *ObjectValveGetInternalServerError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(500)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
