// Code generated by go-swagger; DO NOT EDIT.

package valve

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// ObjectValveLvlPostHandlerFunc turns a function with the right signature into a object valve lvl post handler
type ObjectValveLvlPostHandlerFunc func(ObjectValveLvlPostParams) middleware.Responder

// Handle executing the request and returning a response
func (fn ObjectValveLvlPostHandlerFunc) Handle(params ObjectValveLvlPostParams) middleware.Responder {
	return fn(params)
}

// ObjectValveLvlPostHandler interface for that can handle valid object valve lvl post params
type ObjectValveLvlPostHandler interface {
	Handle(ObjectValveLvlPostParams) middleware.Responder
}

// NewObjectValveLvlPost creates a new http.Handler for the object valve lvl post operation
func NewObjectValveLvlPost(ctx *middleware.Context, handler ObjectValveLvlPostHandler) *ObjectValveLvlPost {
	return &ObjectValveLvlPost{Context: ctx, Handler: handler}
}

/*
ObjectValveLvlPost swagger:route POST /object/valve/lvl valve objectValveLvlPost

Запрос на перекрытие вентиля
*/
type ObjectValveLvlPost struct {
	Context *middleware.Context
	Handler ObjectValveLvlPostHandler
}

func (o *ObjectValveLvlPost) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewObjectValveLvlPostParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}

// ObjectValveLvlPostBody object valve lvl post body
//
// swagger:model ObjectValveLvlPostBody
type ObjectValveLvlPostBody struct {

	// id
	ID uint64 `json:"id,omitempty"`

	// value
	Value float64 `json:"value,omitempty"`
}

// Validate validates this object valve lvl post body
func (o *ObjectValveLvlPostBody) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (o *ObjectValveLvlPostBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *ObjectValveLvlPostBody) UnmarshalBinary(b []byte) error {
	var res ObjectValveLvlPostBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
