package handlers

import (
	"service/internal/features/logging"
	"service/internal/gen/restapi/operations"
	"service/internal/gen/restapi/operations/input"
	"service/internal/gen/restapi/operations/output"
	"service/internal/gen/restapi/operations/pipe_connector"
	"service/internal/gen/restapi/operations/separator"
	"service/internal/gen/restapi/operations/shema"
	"service/internal/gen/restapi/operations/valve"
	"service/internal/schema"
)

type Handler struct {
	s   *schema.Schema
	log logging.Logger
}

func NewHandler(s *schema.Schema, log logging.Logger) *Handler {
	return &Handler{
		s:   s,
		log: log,
	}
}

func (h *Handler) Register(api *operations.SchemaCalcServiceAPI) {
	// input
	api.InputObjectInputGetHandler = input.ObjectInputGetHandlerFunc(h.ObjectInputGet)

	// output
	api.OutputObjectOutputGetHandler = output.ObjectOutputGetHandlerFunc(h.ObjectOutputGet)

	// pipe connector
	api.PipeConnectorObjectPipeConnectorGetHandler = pipe_connector.ObjectPipeConnectorGetHandlerFunc(h.ObjectPipeConnectorGet)

	// separator
	api.SeparatorObjectSeparatorGetHandler = separator.ObjectSeparatorGetHandlerFunc(h.ObjectSeparatorGet)

	// valve
	api.ValveObjectValveGetHandler = valve.ObjectValveGetHandlerFunc(h.ObjectValveGet)

	api.ShemaShemaMetricsGetHandler = shema.ShemaMetricsGetHandlerFunc(h.ShemaMetricsGet)

	api.ValveObjectValveLvlPostHandler = valve.ObjectValveLvlPostHandlerFunc(h.ObjectValveLvl_POST)
}
