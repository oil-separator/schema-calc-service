package handlers

import (
	"service/internal/gen/models"
	"service/internal/gen/restapi/operations/input"

	"github.com/go-openapi/runtime/middleware"
)

func (h *Handler) ObjectInputGet(params input.ObjectInputGetParams) middleware.Responder {
	i := h.s.GetInput()

	out := []*models.Input{}

	for _, v := range i {
		content := []*models.InputContentItems0{}
		for _, content_v := range v.Content {
			item := &models.InputContentItems0{
				Amount: content_v.Amount,
				Value:  content_v.ValueType,
			}
			content = append(content, item)
		}

		item := &models.Input{
			Content: content,
			ID:      v.Id,
			XCord:   v.X,
			YCord:   v.Y,
			ZIndex:  v.Z,
		}

		out = append(out, item)
	}

	return input.NewObjectInputGetOK().WithPayload(out)
}
