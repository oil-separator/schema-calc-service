package handlers

import (
	"service/internal/gen/models"
	"service/internal/gen/restapi/operations/output"

	"github.com/go-openapi/runtime/middleware"
)

func (h *Handler) ObjectOutputGet(params output.ObjectOutputGetParams) middleware.Responder {
	i := h.s.GetOutput()

	out := []*models.Output{}

	for _, v := range i {
		oov := []*models.OutputOutputValuesItems0{}
		for _, oov_v := range v.OutLetContent {
			item := &models.OutputOutputValuesItems0{
				Flow:  oov_v.Amount,
				Value: oov_v.ValueType,
			}
			oov = append(oov, item)
		}

		item := &models.Output{
			ID:           v.Id,
			OutputValues: oov,
			XCord:        v.X,
			YCord:        v.Y,
			ZIndex:       v.Z,
		}

		out = append(out, item)
	}
	return output.NewObjectOutputGetOK().WithPayload(out)
}
