package handlers

import (
	"service/internal/gen/models"
	"service/internal/gen/restapi/operations/pipe_connector"

	"github.com/go-openapi/runtime/middleware"
)

func (h *Handler) ObjectPipeConnectorGet(params pipe_connector.ObjectPipeConnectorGetParams) middleware.Responder {
	getPipeConnectorOut := h.s.GetPipeConnector()

	out := []*models.PipeConnector{}

	for _, v := range getPipeConnectorOut {
		item := &models.PipeConnector{
			ID:                v.Id,
			ObjectIDInput:     v.InLetId,
			ObjectIDOutput:    v.OutLetId,
			ObjectInputPoint:  v.InLetPoint,
			ObjectOutputPoint: v.OutLetPoint,
		}

		out = append(out, item)
	}

	return pipe_connector.NewObjectPipeConnectorGetOK().WithPayload(out)
}
