package handlers

import (
	"service/internal/gen/models"
	"service/internal/gen/restapi/operations/separator"

	"github.com/go-openapi/runtime/middleware"
)

func (h *Handler) ObjectSeparatorGet(params separator.ObjectSeparatorGetParams) middleware.Responder {
	getSeparatorOut := h.s.GetSeparator()

	out := []*models.Separator{}

	for _, v := range getSeparatorOut {
		separatorContent := []*models.SeparatorSeparatorContentItems0{}
		for _, separatorContent_v := range v.SeparatorContent {
			item := &models.SeparatorSeparatorContentItems0{
				Amount: separatorContent_v.Amount,
				Value:  separatorContent_v.ValueType,
			}
			separatorContent = append(separatorContent, item)
		}

		afterBorderSeparatorContent := []*models.SeparatorAfterBorderSeparatorContentItems0{}
		for _, content_v := range v.AfterBorderSeparatorContent {
			item := &models.SeparatorAfterBorderSeparatorContentItems0{
				Amount: content_v.Amount,
				Value:  content_v.ValueType,
			}
			afterBorderSeparatorContent = append(afterBorderSeparatorContent, item)
		}

		item := &models.Separator{
			ID:                          v.Id,
			SeparatorContentCap:         v.SeparatorContentCap,
			SeparatorContent:            separatorContent,
			AfterBorderSeparatorContent: afterBorderSeparatorContent,
		}

		out = append(out, item)
	}

	return separator.NewObjectSeparatorGetOK().WithPayload(out)
}
