package handlers

import (
	"service/internal/gen/restapi/operations/valve"

	"github.com/go-openapi/runtime/middleware"
)

func (h *Handler) ObjectValveGet(params valve.ObjectValveGetParams) middleware.Responder {
	return valve.NewObjectValveGetOK().WithPayload(h.s.GetValveObject())
}
