package handlers

import (
	"service/internal/gen/restapi/operations/valve"

	"github.com/go-openapi/runtime/middleware"
)

func (h *Handler) ObjectValveLvl_POST(params valve.ObjectValveLvlPostParams) middleware.Responder {
	h.s.BlockValve(params.Body.ID, params.Body.Value)

	return valve.NewObjectValveGetOK()
}
