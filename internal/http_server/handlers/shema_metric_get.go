package handlers

import (
	"service/internal/gen/restapi/operations/shema"

	"github.com/go-openapi/runtime/middleware"
)

func (h *Handler) ShemaMetricsGet(params shema.ShemaMetricsGetParams) middleware.Responder {
	return shema.NewShemaMetricsGetOK().WithPayload(h.s.GetInfoObject())
}
