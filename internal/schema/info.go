package schema

import "github.com/pkg/errors"

type info struct {
	id uint64

	f func() error

	value float64
}

func (i *info) Run() error {
	err := i.f()
	if err != nil {
		err = errors.Wrap(err, "[err := i.f()]")
		return err
	}

	return nil
}

func (i *info) SetValue(v float64) {
	i.value = v
}

func (i *info) SetFunc(f func() error) {
	i.f = f
}

func (i *info) Info() float64 {
	return i.value
}

func newInfo(
	id uint64,
) *info {
	return &info{
		id: id,
	}
}
