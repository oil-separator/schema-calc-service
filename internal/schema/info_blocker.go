package schema

type infoBlocker struct {
	id         uint64
	valveBlock *valve

	value float64
}

func (i *infoBlocker) SetValue(value float64) {
	i.value = value
}

func (i *infoBlocker) Block(value float64) error {
	err := i.valveBlock.Block(value)
	if err != nil {
		return err
	}

	return nil
}

func (i *infoBlocker) Info() float64 {
	return i.value
}

func newInfoBlocker(
	id uint64,
) *info {
	return &info{
		id: id,
	}
}
