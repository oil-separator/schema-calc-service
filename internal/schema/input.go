package schema

import (
	"errors"
)

type input struct {
	id uint64

	flow   map[ValueType]float64
	genNum uint64
}

func (pc *input) ID() uint64 {
	return pc.id
}

func (i *input) Write(let letter) {
	content := let.GetContent()

	if let.GetGenNum() != i.genNum {
		i.genNum = let.GetGenNum()
		i.flow = make(map[ValueType]float64)
	}

	for valueType, amount := range content {
		i.flow[valueType] += amount
	}
}

func (i *input) Connect(point uint64, pc *pipeConnector) error {
	return errors.New("unsupport operation")
}

func (i *input) GetBlockLvl() float64 {
	return 1
}

type inputOut_Content struct {
	ValueType string
	Amount    float64
}

type inputOut struct {
	Id      uint64
	X       float64
	Y       float64
	Z       uint64
	Content []inputOut_Content
}

func (i *input) Info() inputOut {
	iP_C := []inputOut_Content{}
	for key, v := range i.flow {
		item := inputOut_Content{
			ValueType: key.value,
			Amount:    v,
		}

		iP_C = append(iP_C, item)
	}

	return inputOut{
		Id:      i.id,
		Content: iP_C,
	}
}

func newInput(
	id uint64,
) *input {
	out := &input{
		id: id,

		flow: make(map[ValueType]float64),
	}

	return out
}
