package schema

import (
	"fmt"
)

type output struct {
	id uint64

	genNum uint64
	flow   map[ValueType]float64

	outLet []*pipeConnector
}

func (o *output) ID() uint64 {
	return o.id
}

func (o *output) GetBlockLvl() float64 {
	return 1
}

func (o *output) Write(let letter) {
	content := let.GetContent()

	if let.GetGenNum() != o.genNum {
		o.genNum = let.GetGenNum()
		o.flow = make(map[ValueType]float64)
	}

	for valueType, amount := range content {
		o.flow[valueType] += amount
	}

	connected := getConnected(o.outLet)
	maxBlockLvl, sum := getMaxBlockLvlAndSum(connected)

	for _, val := range connected {
		newLet := NewLetter(let.GetGenNum())

		blockLvl, _ := val.GetBlockLvl()

		for valueType, amount := range content {
			var needSend float64
			if sum != 0 {
				needSend = ((amount / sum) * maxBlockLvl) * blockLvl
			} else {
				needSend = 0
			}
			newLet.AddContent(valueType, needSend)
		}

		val.Write(*newLet)
	}
}

func (o *output) Connect(point uint64, pc *pipeConnector) error {
	if point != 0 {
		return fmt.Errorf("cannot connect to %d point, only 0 point suppport", point)
	}

	pc.outLet = o
	pc.outLetPoint = point

	o.outLet = append(o.outLet, pc)

	return nil
}

type outputOut_OutLetContent struct {
	ValueType string
	Amount    float64
}

type outputOut struct {
	Id            uint64
	X             float64
	Y             float64
	Z             uint64
	OutLetContent []outputOut_OutLetContent
}

func (o *output) Info() outputOut {
	oO_OLC := []outputOut_OutLetContent{}

	for key, v := range o.flow {
		item := outputOut_OutLetContent{
			ValueType: key.value,
			Amount:    v,
		}
		oO_OLC = append(oO_OLC, item)

	}

	return outputOut{
		Id:            o.id,
		OutLetContent: oO_OLC,
	}
}

func newOutput(
	id uint64,
) *output {
	o := &output{
		id:   id,
		flow: make(map[ValueType]float64),
	}

	return o
}
