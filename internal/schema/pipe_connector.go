package schema

type pipeConnector struct {
	id uint64

	inLet       ObjectInterface
	inLetPoint  uint64
	outLet      ObjectInterface
	outLetPoint uint64
}

func (pc *pipeConnector) ID() uint64 {
	return pc.id
}

func (pc *pipeConnector) Write(let letter) bool {
	if pc.inLet != nil {
		pc.inLet.Write(let)
		return true
	}

	return false
}

func (pc *pipeConnector) GetBlockLvl() (float64, bool) {
	if pc.inLet != nil {
		return pc.inLet.GetBlockLvl(), true
	}

	return 0, false
}

func (pc *pipeConnector) GetInLetPoint() uint64 {
	return pc.inLetPoint
}

func (pc *pipeConnector) GetOutLetPoint() uint64 {
	return pc.outLetPoint
}

func (pc *pipeConnector) Connect(object ObjectInterface, point uint64) {
	pc.inLet = object
	pc.inLetPoint = point
}

type pipeConnectorOut struct {
	Id          uint64
	OutLetId    uint64
	OutLetPoint uint64
	InLetId     uint64
	InLetPoint  uint64
}

func (pc *pipeConnector) Info() pipeConnectorOut {
	return pipeConnectorOut{
		Id:          pc.id,
		OutLetId:    pc.outLet.ID(),
		OutLetPoint: pc.outLetPoint,
		InLetId:     pc.inLet.ID(),
		InLetPoint:  pc.inLetPoint,
	}
}

func newPipeConnector(id uint64) *pipeConnector {
	return &pipeConnector{
		id: id,
	}
}
