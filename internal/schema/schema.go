package schema

import (
	"fmt"
	"service/internal/gen/models"
	"sync"

	"github.com/pkg/errors"
)

func Connect(from uint64, to uint64, c1 ObjectInterface, c2 ObjectInterface) (*pipeConnector, error) {
	pc := newPipeConnector(0)

	err := c1.Connect(from, pc)
	if err != nil {
		err = errors.Wrap(err, "[err := c1.Connect(from, pc)]")
		return nil, err
	}

	pc.Connect(c2, to)

	return pc, nil
}

func getConnected(pc []*pipeConnector) []*pipeConnector {
	newPc := []*pipeConnector{}

	for _, val := range pc {
		_, ok := val.GetBlockLvl()
		if !ok {
			continue
		}
		newPc = append(newPc, val)
	}

	return newPc
}

func getMaxBlockLvlAndSum(pc []*pipeConnector) (maxBlockLvl float64, sum float64) {
	for _, val := range pc {
		blockLvl, ok := val.GetBlockLvl()
		if !ok {
			continue
		}
		if blockLvl > maxBlockLvl {
			maxBlockLvl = blockLvl
		}
		sum += blockLvl
	}

	return
}

type letter struct {
	content       map[ValueType]float64
	generationNum uint64
}

func (l *letter) GetContent() map[ValueType]float64 {
	return l.content
}

func (l *letter) GetGenNum() uint64 {
	return l.generationNum
}

func (l *letter) AddContent(v ValueType, amount float64) {
	a, ok := l.content[v]
	if !ok {
		l.content[v] = amount
	}
	l.content[v] = amount + a
}

func NewLetter(genNum uint64) *letter {
	return &letter{
		content:       map[ValueType]float64{},
		generationNum: genNum,
	}
}

type ObjectInterface interface {
	ID() uint64
	Connect(uint64, *pipeConnector) error
	Write(letter)
	GetBlockLvl() float64
}

type Schema struct {
	inputObjects     []*input
	outputObjects    []*output
	pipeConnectors   []*pipeConnector
	separatorObjects []*separator
	valveObjects     []*valve
	infoObjects      []*info
	locker           sync.Mutex
}

func (s *Schema) NewInput(
	id uint64,
) *input {
	s.locker.Lock()
	defer s.locker.Unlock()

	v := newInput(
		id,
	)

	s.inputObjects = append(s.inputObjects, v)

	return v
}

func (s *Schema) GetInput() []inputOut {
	out := []inputOut{}

	for _, v := range s.inputObjects {
		out = append(out, v.Info())
	}

	return out
}

func (s *Schema) NewOutput(
	id uint64,
) *output {
	s.locker.Lock()
	defer s.locker.Unlock()

	v := newOutput(
		id,
	)

	s.outputObjects = append(s.outputObjects, v)

	return v
}

func (s *Schema) GetOutput() []outputOut {
	out := []outputOut{}

	for _, v := range s.outputObjects {
		out = append(out, v.Info())
	}

	return out
}

func (s *Schema) NewPipeConnector(
	id uint64,
) *pipeConnector {
	s.locker.Lock()
	defer s.locker.Unlock()

	v := newPipeConnector(
		id,
	)

	s.pipeConnectors = append(s.pipeConnectors, v)

	return v
}

func (s *Schema) GetPipeConnector() []pipeConnectorOut {
	out := []pipeConnectorOut{}

	for _, v := range s.pipeConnectors {
		out = append(out, v.Info())
	}

	return out
}

func (s *Schema) NewSeparator(
	id uint64,
	// TODO: сделать поск по id, но уже когда будет БД
	separatorContentCapacity float64,
) *separator {
	s.locker.Lock()
	defer s.locker.Unlock()

	v := newSeparator(
		id,
		separatorContentCapacity,
	)

	s.separatorObjects = append(s.separatorObjects, v)

	return v
}

func (s *Schema) GetSeparator() []separatorOut {
	s.locker.Lock()
	defer s.locker.Unlock()

	out := []separatorOut{}

	for _, v := range s.separatorObjects {
		fmt.Println(v.Info())
		out = append(out, v.Info())
	}

	return out
}

func (s *Schema) NewValve(
	id uint64,
	typ uint64,
) *valve {
	s.locker.Lock()
	defer s.locker.Unlock()

	v := newValve(
		id,
		typ,
	)

	s.valveObjects = append(s.valveObjects, v)

	return v
}

func (s *Schema) BlockValve(
	id uint64,
	value float64,
) {
	s.locker.Lock()
	defer s.locker.Unlock()

	for _, v := range s.valveObjects {
		if v.ID() == id {
			v.Block(value)
		}
	}
}

func (s *Schema) GetValveObject() []*models.Valve {
	out := []*models.Valve{}

	for _, v := range s.valveObjects {
		h := &models.Valve{
			ID:   v.id,
			Open: v.blockLvl,
		}
		out = append(out, h)
	}

	return out
}

func (s *Schema) NewInfo(id uint64) *info {
	s.locker.Lock()
	defer s.locker.Unlock()

	v := newInfo(id)

	s.infoObjects = append(s.infoObjects, v)
	return v
}

func (s *Schema) GetInfoObject() []*models.Metric {
	s.locker.Lock()
	defer s.locker.Unlock()

	out := []*models.Metric{}

	for _, v := range s.infoObjects {
		v := &models.Metric{
			ID:    v.id,
			Value: v.value,
		}

		out = append(out, v)
	}

	return out
}

func NewSchema() *Schema {
	return &Schema{}
}
