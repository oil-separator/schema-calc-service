package schema

import (
	"fmt"
	"math"
)

type separator struct {
	id uint64 // id сепаратора

	genNum uint64
	flow   map[ValueType]float64

	inf []*info

	separatorContentCapacity float64

	borderPipeline []ValueType

	separatorContent map[ValueType]float64
	outBorderContent map[ValueType]float64

	outLet1 []*pipeConnector
	outLet2 []*pipeConnector
	outLet3 []*pipeConnector
	outLet4 []*pipeConnector
	outLet5 []*pipeConnector
}

func (s *separator) OilLevel() float64 {
	var sum float64
	for typ, v := range s.separatorContent {
		if typ == NewGas() {
			continue
		}
		sum += v
	}

	return sum
}

func (s *separator) MezhFaz() float64 {
	var sum float64
	for typ, v := range s.separatorContent {
		if typ == NewGas() {
			continue
		}
		if typ == NewClearOil() {
			continue
		}
		sum += v
	}

	return sum
}

func (s *separator) Davlenie1() float64 {
	var sum float64
	for _, v := range s.separatorContent {
		sum += v
	}

	return s.separatorContentCapacity / sum
}

func (s *separator) Davlenie2() float64 {
	var sum float64
	for _, v := range s.separatorContent {
		sum += v
	}

	return sum / s.separatorContentCapacity
}

func (s *separator) Temp() float64 {
	return 19
}

func (s *separator) PerepadDavl() float64 {
	return s.Davlenie1() - s.Davlenie2()
}

func (s *separator) UrNeftGaz1() float64 {
	var sum float64
	for _, v := range s.separatorContent {
		sum += v
	}

	return sum
}

func (s *separator) UrNeftGaz2() float64 {
	var sum float64
	for _, v := range s.separatorContent {
		sum += v
	}

	return sum * math.Pi
}

func (pc *separator) ID() uint64 {
	return pc.id
}

func (s *separator) calcAfterBorder() {
	var capValue float64 = 0

	// считаем общий объём камеры сепаратора
	for key, v := range s.separatorContent {
		// если газ, то пропускаем
		if key == NewGas() {
			continue
		}
		capValue += v
	}

	if capValue > s.separatorContentCapacity {
		needRemove := capValue - s.separatorContentCapacity
		for _, v := range s.borderPipeline {
			if s.separatorContent[v]-needRemove < 0 {
				needRemove = needRemove - s.separatorContent[v]
				val := s.outBorderContent[v]

				s.outBorderContent[v] = s.separatorContent[v] + val

				s.separatorContent[v] = 0
				continue
			}

			s.separatorContent[v] = s.separatorContent[v] - needRemove
			val := s.outBorderContent[v]
			s.outBorderContent[v] = val + needRemove
			break
		}
	}
}

// первый дренаж из камеры сепаратора
func (s *separator) writePoint1And2() {
	connectedOutLet1 := getConnected(s.outLet1)
	connectedOutLet2 := getConnected(s.outLet2)

	connected := append(connectedOutLet1, connectedOutLet2...)

	maxBlockLvl, sum := getMaxBlockLvlAndSum(connected)

	for _, val := range connected {
		newLet := NewLetter(s.genNum)

		blockLvl, _ := val.GetBlockLvl()

		for _, amount := range s.separatorContent {
			var needGive float64
			if sum != 0 {
				needGive = ((amount / sum) * maxBlockLvl) * blockLvl
			} else {
				needGive = 0
			}

			for i := len(s.borderPipeline) - 1; i >= 0; i-- {
				valu := s.borderPipeline[i]
				if s.separatorContent[valu]-needGive < 0 {
					newLet.AddContent(valu, s.separatorContent[valu])
					needGive = needGive - s.separatorContent[valu]
					s.separatorContent[valu] = 0
					continue
				}
				res := s.separatorContent[valu] - needGive
				newLet.AddContent(valu, needGive)
				s.separatorContent[valu] = res
				break
			}
		}

		val.Write(*newLet)
	}
}

func (s *separator) writePoint3And4() {
	connectedOutLet3 := getConnected(s.outLet3)
	connectedOutLet4 := getConnected(s.outLet4)

	connected := append(connectedOutLet3, connectedOutLet4...)

	maxBlockLvl, sum := getMaxBlockLvlAndSum(connected)

	for _, val := range connected {
		newLet := NewLetter(s.genNum)

		blockLvl, _ := val.GetBlockLvl()

		for _, amount := range s.outBorderContent {
			var needGive float64
			if sum != 0 {
				needGive = ((amount / sum) * maxBlockLvl) * blockLvl
			} else {
				needGive = 0
			}

			for i := len(s.borderPipeline) - 1; i >= 0; i-- {
				valu := s.borderPipeline[i]
				if s.outBorderContent[valu]-needGive < 0 {
					newLet.AddContent(valu, s.outBorderContent[valu])
					needGive = needGive - s.outBorderContent[valu]
					s.outBorderContent[valu] = 0
					continue
				}
				res := s.outBorderContent[valu] - needGive
				newLet.AddContent(valu, needGive)
				s.outBorderContent[valu] = res
				break
			}
		}

		val.Write(*newLet)
	}
}

func (s *separator) writePoint5() {
	connected := getConnected(s.outLet5)
	maxBlockLvl, sum := getMaxBlockLvlAndSum(connected)

	for _, val := range connected {
		newLet := NewLetter(s.genNum)

		blockLvl, _ := val.GetBlockLvl()

		amount := s.separatorContent[NewGas()]

		var needGive float64
		if sum != 0 {
			needGive = ((amount / sum) * maxBlockLvl) * blockLvl
		} else {
			needGive = 0
		}
		newLet.AddContent(NewGas(), needGive)

		val.Write(*newLet)
	}
}

func (s *separator) Write(let letter) {
	letContent := let.GetContent()

	if let.GetGenNum() != s.genNum {
		s.genNum = let.GetGenNum()
		s.flow = make(map[ValueType]float64)
	}

	for key, v := range letContent {
		s.flow[key] += v

		val := s.separatorContent[key]

		switch key {
		case NewCrudeOil():
			gasVal := s.separatorContent[NewGas()]
			clearOilVal := s.separatorContent[NewClearOil()]
			waterVal := s.separatorContent[NewWater()]

			s.separatorContent[NewGas()] = gasVal + v/100*10
			s.separatorContent[NewClearOil()] = clearOilVal + v/100*70
			s.separatorContent[NewWater()] = waterVal + v/100*20
			continue
		}

		s.separatorContent[key] = val + v
	}

	s.calcAfterBorder()
	s.writePoint1And2()
	s.writePoint3And4()
	s.writePoint5()
	s.infoPro()
}

func (s *separator) GetBlockLvl() float64 {
	return 1
}

func (s *separator) infoPro() {
	for _, v := range s.inf {
		v.Run()
	}
}

func (s *separator) Connect(point uint64, pc *pipeConnector) error {
	if point < 1 || 5 < point {
		return fmt.Errorf("cannot connect to %d point, only 1,2,3 point suppport", point)
	}

	pc.outLet = s
	pc.outLetPoint = point

	switch point {
	case 1:
		s.outLet1 = append(s.outLet1, pc)
	case 2:
		s.outLet2 = append(s.outLet2, pc)
	case 3:
		s.outLet3 = append(s.outLet3, pc)
	case 4:
		s.outLet4 = append(s.outLet4, pc)
	case 5:
		s.outLet5 = append(s.outLet5, pc)
	}

	return nil
}

type separatorOut_SeparatorContent struct {
	ValueType string
	Amount    float64
}

type separatorOut_AfterBorderSeparatorContent struct {
	ValueType string
	Amount    float64
}

type separatorOut struct {
	Id                          uint64
	SeparatorContentCap         float64
	SeparatorContent            []separatorOut_SeparatorContent
	AfterBorderSeparatorContent []separatorOut_AfterBorderSeparatorContent
}

func (i *separator) Info() separatorOut {
	sO_SC := []separatorOut_SeparatorContent{}
	for key, v := range i.separatorContent {
		item := separatorOut_SeparatorContent{
			ValueType: key.value,
			Amount:    v,
		}

		sO_SC = append(sO_SC, item)
	}

	sO_ABSC := []separatorOut_AfterBorderSeparatorContent{}
	for key, v := range i.outBorderContent {
		item := separatorOut_AfterBorderSeparatorContent{
			ValueType: key.value,
			Amount:    v,
		}

		sO_ABSC = append(sO_ABSC, item)
	}

	return separatorOut{
		Id:                          i.id,
		SeparatorContentCap:         i.separatorContentCapacity,
		SeparatorContent:            sO_SC,
		AfterBorderSeparatorContent: sO_ABSC,
	}
}

func (s *separator) AddInfo(inf *info) {
	s.inf = append(s.inf, inf)
}

func newSeparator(
	id uint64,
	separatorContentCapacity float64,
) *separator {
	out := &separator{
		id:                       id,
		separatorContentCapacity: separatorContentCapacity,

		borderPipeline: []ValueType{
			NewClearOil(),
			NewCrudeOil(),
			NewWater(),
		},

		flow: make(map[ValueType]float64),

		separatorContent: make(map[ValueType]float64),
		outBorderContent: make(map[ValueType]float64),
	}

	return out
}
