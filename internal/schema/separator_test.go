package schema

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSeparatorOverload(t *testing.T) {
	var n float64 = 10000

	separator := newSeparator(0, n)

	as := assert.New(t)

	let := NewLetter(0)
	let.AddContent(NewWater(), n)
	let.AddContent(NewCrudeOil(), n)

	separator.Write(*let)

	as.Equal(separator.flow[NewWater()], n)
	as.Equal(separator.flow[NewCrudeOil()], n)

	as.Equal(separator.separatorContent[NewGas()], n/10)
	as.Equal(separator.separatorContent[NewCrudeOil()], n*0)
	as.Equal(separator.separatorContent[NewWater()], n)

	as.Equal(separator.outBorderContent[NewWater()], n/100*20)
	as.Equal(separator.outBorderContent[NewClearOil()], n/100*70)
}
