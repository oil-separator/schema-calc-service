package schema

type ValueType struct {
	value string
}

func NewWater() ValueType {
	return ValueType{
		value: "water",
	}
}

func NewCrudeOil() ValueType {
	return ValueType{
		value: "crude-oil",
	}
}

func NewClearOil() ValueType {
	return ValueType{
		value: "clear-oil",
	}
}

func NewGas() ValueType {
	return ValueType{
		value: "gas",
	}
}
