package schema

import "fmt"

type valve struct {
	id         uint64
	outfitType uint64

	inf *info

	blockLvl float64

	genNum uint64
	flow   map[ValueType]float64

	outLet []*pipeConnector
}

func (v *valve) ID() uint64 {
	return v.id
}

func (v *valve) Connect(point uint64, pc *pipeConnector) error {
	if 3 < point {
		return fmt.Errorf("cannot connect to %d point, only 0,1,2,3 point suppport", point)
	}

	pc.outLet = v
	pc.outLetPoint = point

	v.outLet = append(v.outLet, pc)

	return nil
}

func (v *valve) Write(let letter) {
	content := let.GetContent()

	if let.GetGenNum() != v.genNum {
		v.genNum = let.GetGenNum()
		v.flow = make(map[ValueType]float64)
	}

	for valueType, amount := range content {
		v.flow[valueType] += amount
	}

	connected := getConnected(v.outLet)
	maxBlockLvl, sum := getMaxBlockLvlAndSum(connected)

	for _, val := range connected {
		newLet := NewLetter(let.GetGenNum())

		blockLvl, _ := val.GetBlockLvl()

		for valueType, amount := range content {
			var needGive float64
			if sum != 0 {
				needGive = ((amount / sum) * maxBlockLvl) * blockLvl
			} else {
				needGive = 0
			}
			newLet.AddContent(valueType, needGive)
		}

		val.Write(*newLet)
	}

	if v.inf != nil {
		v.inf.Run()
	}
}

func (v *valve) SetInfo(inf *info) {
	v.inf = inf
}

func (v *valve) GetBlockLvl() float64 {
	return v.blockLvl
}

func (v *valve) Block(n float64) error {
	v.blockLvl = n
	// TODO: сделать проверку на 0 < n < 1
	return nil
}

type valveOut struct {
}

func (v *valve) Info() valveOut {
	return valveOut{}
}

func (v *valve) Add(inf *info) {
	v.inf = inf
}

func newValve(id uint64, outfitType uint64) *valve {
	v := &valve{
		id:         id,
		outfitType: outfitType,
		flow:       make(map[ValueType]float64),
	}

	return v
}
