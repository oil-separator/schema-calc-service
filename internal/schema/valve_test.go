package schema

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValve(t *testing.T) {
	s := NewSchema()

	as := assert.New(t)

	out := s.NewOutput(0)

	v1 := s.NewValve(0, 0)
	err := v1.Block(0.5)
	if err != nil {
		t.Error(err)
	}

	v2 := s.NewValve(1, 0)
	err = v2.Block(0.5)
	if err != nil {
		t.Error(err)
	}

	v3 := s.NewValve(2, 0)
	err = v3.Block(0.5)
	if err != nil {
		t.Error(err)
	}

	_, err = Connect(0, 0, out, v1)
	if err != nil {
		t.Error(err)
	}

	_, err = Connect(0, 0, out, v2)
	if err != nil {
		t.Error(err)
	}

	_, err = Connect(0, 0, out, v3)
	if err != nil {
		t.Error(err)
	}

	imp := s.NewInput(0)

	_, err = Connect(0, 0, v1, imp)
	if err != nil {
		t.Error(err)
	}

	_, err = Connect(0, 0, v2, imp)
	if err != nil {
		t.Error(err)
	}

	_, err = Connect(0, 0, v3, imp)
	if err != nil {
		t.Error(err)
	}

	let := NewLetter(0)
	let.AddContent(NewWater(), 200)

	out.Write(*let)

	as.Equal(float64(100), imp.flow[NewWater()])

	as.Equal(float64(100)/float64(3), v1.flow[NewWater()])
	as.Equal(float64(100)/float64(3), v2.flow[NewWater()])
	as.Equal(float64(100)/float64(3), v3.flow[NewWater()])
}
